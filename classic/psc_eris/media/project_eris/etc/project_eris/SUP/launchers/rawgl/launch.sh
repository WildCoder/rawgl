#!/bin/sh
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "rawgl"
echo -n 2 > "/data/power/disable"
for f in "gamedata/"*".iso"; do
    [ -e "${f}" ] || continue
    chmod +x convert_3do
    ${PROJECT_ERIS_PATH}/bin/sdl_text_display "Converting 3DO Another World ISO to playable format" &
    cp -f "convert_3do" "gamedata/"
    cd "gamedata/"
    ./convert_3do "../${f}"
    killall sdl_display
    [ -d "GameData" ] && rm *".iso" && rm convert_3do
    cd ..
done
HOME="/var/volatile/launchtmp" LD_LIBRARY_PATH="${PROJECT_ERIS_PATH}/lib" SDL_GAMECONTROLLERCONFIG="$(cat ${PROJECT_ERIS_PATH}/etc/boot_menu/gamecontrollerdb.txt)" ./rawgl --window=1280x720 --render=software --datapath="$(pwd)/gamedata" &> "${RUNTIME_LOG_PATH}/anotherworld.log" 
echo -n 1 > "/data/power/disable"
echo "launch_StockUI" > "/tmp/launchfilecommand"